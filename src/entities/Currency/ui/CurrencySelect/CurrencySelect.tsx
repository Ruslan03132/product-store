import { memo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { styled } from "@mui/material";

import { MuiSelect } from "shared/ui/Select/MuiSelect";

import { Currency } from "../../model/types/currency";

interface CurrencySelectProps {
    value?: Currency;
    onChange?: (value: Currency) => void;
    readonly?: boolean;
}

const options = [
    { value: Currency.RUB, content: Currency.RUB },
    { value: Currency.EUR, content: Currency.EUR },
    { value: Currency.USD, content: Currency.USD },
];

export const CurrencySelect = memo(
    ({ value, onChange, readonly }: CurrencySelectProps) => {
        const { t } = useTranslation("profile");

        const onChangeHandler = useCallback(
            (value: string) => {
                onChange?.(value as Currency);
            },
            [onChange],
        );

        return (
            <MuiSelect
                label={t("Укажите валюту")}
                options={options}
                value={value}
                onChange={onChangeHandler}
                readonly={readonly}
            />
        );
    },
);
