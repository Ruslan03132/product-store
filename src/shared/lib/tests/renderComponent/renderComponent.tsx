import { ReactNode } from "react";
import { I18nextProvider } from "react-i18next";
import { MemoryRouter } from "react-router-dom";
import { render } from "@testing-library/react";

import { StateSchema, StoreProvider } from "app/providers/StoreProvider";
import { i18nForTest } from "shared/config/i18n/i18nForTest";

import { DeepPartial } from "../deepPartial";

export interface renderWithOptions {
    route?: string;
    initialState?: DeepPartial<StateSchema>;
}

export const RenderComponent = (
    component: ReactNode,
    options: renderWithOptions = { route: "/" },
) => {
    const { route = "/", initialState } = options;
    return render(
        <MemoryRouter initialEntries={[route]}>
            <StoreProvider initialState={initialState as StateSchema}>
                <I18nextProvider i18n={i18nForTest}>
                    {component}
                </I18nextProvider>
            </StoreProvider>
        </MemoryRouter>,
    );
};
