import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { AppLink, AppLinkTheme } from "./AppLink";

import "app/styles/index.scss";

const meta = {
    title: "widgets/AppLink",
    component: AppLink,
    tags: ["autodocs"],
    args: { to: "/" },
} satisfies Meta<typeof AppLink>;

export default meta;

type Story = StoryObj<typeof meta>;

export const invertedPrimary: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.INVERTED_PRIMARY,
    },
};

export const invertedSecondary: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.INVERTED_SECONDARY,
    },
};

export const primary: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.PRIMARY,
    },
};

export const invertedPrimaryDark: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.INVERTED_PRIMARY,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const invertedSecondaryDark: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.INVERTED_SECONDARY,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const primaryDark: Story = {
    args: {
        children: "Text",
        theme: AppLinkTheme.PRIMARY,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};
