import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { Button, ButtonSize, ButtonTheme } from "./Button";

import "app/styles/index.scss";

const meta = {
    title: "shared/Button",
    component: Button,
    tags: ["autodocs"],
} satisfies Meta<typeof Button>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: "Primary",
    },
};

export const Clear: Story = {
    args: {
        theme: ButtonTheme.CLEAR,
        children: "Secondary",
    },
};

export const ClearInverted: Story = {
    args: {
        theme: ButtonTheme.CLEAR_INVERTED,
        children: "Secondary",
    },
};

export const Outline: Story = {
    args: { theme: ButtonTheme.OUTLINE, children: "Outline" },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const OutlineSizeL: Story = {
    args: {
        theme: ButtonTheme.OUTLINE,
        children: "Outline",
        size: ButtonSize.L,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const OutlineSizeXL: Story = {
    args: {
        theme: ButtonTheme.OUTLINE,
        children: "Outline",
        size: ButtonSize.XL,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const Background: Story = {
    args: { theme: ButtonTheme.BACKGROUND, children: "Background" },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const BackgroundInverted: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERTED,
        children: "BackgroundInverted",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const Square: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERTED,
        square: true,
        children: ">",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const SquareL: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERTED,
        square: true,
        size: ButtonSize.L,
        children: ">",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const SquareXL: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERTED,
        square: true,
        size: ButtonSize.XL,
        children: ">",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const Disabled: Story = {
    args: {
        theme: ButtonTheme.OUTLINE,
        size: ButtonSize.M,
        disabled: true,
        children: "Disabled",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};
