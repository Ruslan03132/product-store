import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { CountrySelect } from "./CountrySelect";

const meta = {
    title: "entities/CurrencySelect",
    component: CountrySelect,
    tags: ["autodocs"],
} satisfies Meta<typeof CountrySelect>;

export default meta;

type Story = StoryObj<typeof meta>;

export const CurrencySelect: Story = {
    args: {},
};

export const CurrencySelectDark: Story = {
    args: {},
    decorators: [ThemeDecorator(Theme.DARK)],
};
