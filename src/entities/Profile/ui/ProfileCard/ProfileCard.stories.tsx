import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import avatar from "shared/assets/tests/testwoman.jpg";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { ProfileCard } from "./ProfileCard";

import "app/styles/index.scss";

const meta = {
    title: "entities/ProfileCard",
    component: ProfileCard,
    tags: ["autodocs"],
} satisfies Meta<typeof ProfileCard>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ProfileCardLight: Story = {
    args: {
        data: {
            username: "admin",
            age: 22,
            country: Country.Armenia,
            lastname: "borisvch",
            first: "ruslan",
            city: "acs",
            currency: Currency.EUR,
            avatar,
        },
    },
};

export const ProfileCardWithError: Story = {
    args: { error: "error" },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const ProfileCardDarkIsLoading: Story = {
    args: { isLoading: true },
    decorators: [ThemeDecorator(Theme.DARK)],
};
