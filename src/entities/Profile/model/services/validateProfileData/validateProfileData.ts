import { Profile, ValidateProfileError } from "../../types/profile";

export const validateProfileData = (
    profile?: Profile,
): ValidateProfileError[] => {
    const errors: ValidateProfileError[] = [];

    if (!profile) {
        errors.push(ValidateProfileError.NO_DATA);
        return errors;
    }

    const { first, lastname, age, country, city } = profile as Profile;

    if (!first || !lastname) {
        errors.push(ValidateProfileError.INCORRECT_USER_DATA);
    }

    if (!age || !Number.isInteger(age)) {
        errors.push(ValidateProfileError.INCORRECT_AGE);
    }

    if (!country) {
        errors.push(ValidateProfileError.INCORRECT_COUNTRY);
    }

    if (!city) {
        errors.push(ValidateProfileError.NO_CITY);
    }

    return errors;
};
