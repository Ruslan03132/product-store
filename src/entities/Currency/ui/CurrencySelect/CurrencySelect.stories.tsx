import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { CurrencySelect } from "./CurrencySelect";

const meta = {
    title: "entities/CurrencySelect",
    component: CurrencySelect,
    tags: ["autodocs"],
} satisfies Meta<typeof CurrencySelect>;

export default meta;

type Story = StoryObj<typeof meta>;

export const PrimaryModal: Story = {
    args: {},
};

export const PrimaryModalDark: Story = {
    args: {},
    decorators: [ThemeDecorator(Theme.DARK)],
};
