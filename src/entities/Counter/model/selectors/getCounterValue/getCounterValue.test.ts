import { StateSchema } from "app/providers/StoreProvider";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getCounterValue } from "./getCounterValue";

describe("getCounterValue", () => {
    test("getvalue", () => {
        const state: DeepPartial<StateSchema> = {
            counter: { value: 10 },
        };
        expect(getCounterValue(state as StateSchema)).toEqual(10);
    });
});
