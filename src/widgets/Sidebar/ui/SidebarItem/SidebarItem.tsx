import { memo } from "react";
import { useSelector } from "react-redux";
import { t } from "i18next";

import { SidebarItemType } from "widgets/Sidebar/model/types/items";
import { getUserAuthData } from "entities/User";
import { classNames } from "shared/lib/classNames/classNames";
import { AppLink, AppLinkTheme } from "shared/ui/AppLink/AppLink";

import styles from "./SidebarItem.module.scss";

interface SidebarItemProps {
    item: SidebarItemType;
    collapsed: boolean;
}

export const SidebarItem = memo(({ item, collapsed }: SidebarItemProps) => {
    const { path, text, Icon } = item;

    const isAuth = useSelector(getUserAuthData);

    if (item.authOnly && !isAuth) {
        return null;
    }

    return (
        <AppLink
            to={path}
            theme={AppLinkTheme.INVERTED_PRIMARY}
            className={classNames(styles.linkWrap, {
                [styles.collapsed]: collapsed,
            })}
        >
            <Icon className={styles.icon} />
            <span className={styles.link}>{t(text)}</span>
        </AppLink>
    );
});
