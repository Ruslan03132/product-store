import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { Modal } from "./Modal";

const meta = {
    title: "shared/Modal",
    component: Modal,
    tags: ["autodocs"],
} satisfies Meta<typeof Modal>;

export default meta;

type Story = StoryObj<typeof meta>;

export const PrimaryModal: Story = {
    args: {
        children:
            "sefseffffffffffffffseeeeeeeeeeeeee eeefesfsfffffffffffffffff efsefessssssssssssssssssssssssssssssssssss",
        isOpen: true,
    },
};

export const PrimaryModalDark: Story = {
    args: {
        children:
            "sefseffffffffffffffseeeeeeeeeeeeee eeefesfsfffffffffffffffff efsefessssssssssssssssssssssssssssssssssss",
        isOpen: true,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};
