import { memo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";

import {
    DynamicModuleLoader,
    ReducersList,
} from "shared/lib/components/DynamicModuleLoader/DynamicModuleLoader";
import { useAppDispatch } from "shared/lib/hooks/useAppDispatch/useAppDispatch";
import { Button, ButtonTheme } from "shared/ui/Button/Button";
import { Input } from "shared/ui/Input/Input";
import { Text, TextTheme } from "shared/ui/Text/Text";

import { getLoginError } from "../../model/selectors/getLoginError/getLoginError";
import { getLoginIsLoading } from "../../model/selectors/getLoginIsLoading/getLoginIsLoading";
import { getLoginPassword } from "../../model/selectors/getLoginPassword/getLoginPassword";
import { getLoginUsername } from "../../model/selectors/getLoginUsername/getLoginUsername";
import { loginByUsername } from "../../model/services/loginByUsername/loginByUsername";
import { loginActions, loginReducer } from "../../model/slice/loginSlice";

export interface LoginFormProps {
    className?: string;
    onSuccess: () => void;
}

const StyledButton = styled(Button)`
    margin-top: 15px;
    margin-left: auto;
`;

const StyledInput = memo(styled(Input)`
    margin-top: 10px;
`);

const StyledLoginForm = styled.div`
    display: flex;
    flex-direction: column;
    width: 400px;
`;

const initialReducers: ReducersList = {
    loginForm: loginReducer,
};

const LoginForm = memo((props: LoginFormProps) => {
    const { onSuccess } = props;
    const { t } = useTranslation();
    const dispatch = useAppDispatch();

    const username = useSelector(getLoginUsername);
    const password = useSelector(getLoginPassword);
    const isLoading = useSelector(getLoginIsLoading);
    const error = useSelector(getLoginError);

    const onChangeUsername = useCallback(
        (value: string) => {
            dispatch(loginActions.setUsername(value));
        },
        [dispatch],
    );
    const onChangePassword = useCallback(
        (value: string) => {
            dispatch(loginActions.setPassword(value));
        },
        [dispatch],
    );

    const onLoginClick = useCallback(async () => {
        // @ts-ignore
        const result = await dispatch(loginByUsername({ username, password }));

        if (result.meta.requestStatus === "fulfilled") {
            onSuccess();
        }
    }, [onSuccess, dispatch, username, password]);

    return (
        <DynamicModuleLoader reducers={initialReducers} removeAfterUnmount>
            <StyledLoginForm {...props}>
                <Text title={t("Форма авторизации")} />
                {error && (
                    <Text
                        styledComponentTheme={TextTheme.ERROR}
                        text={t("Вы ввели неверный логин или пароль")}
                    />
                )}
                <StyledInput
                    onChange={onChangeUsername}
                    placeholder={t("Введите логин")}
                    autoFocus
                    value={username}
                />
                <StyledInput
                    onChange={onChangePassword}
                    placeholder={t("Введите пароль")}
                    value={password}
                />
                <StyledButton
                    styledComponentTheme={ButtonTheme.BACKGROUND_INVERTED}
                    onClick={onLoginClick}
                    disabled={isLoading}
                >
                    {t("Войти")}
                </StyledButton>
            </StyledLoginForm>
        </DynamicModuleLoader>
    );
});

export default LoginForm;
