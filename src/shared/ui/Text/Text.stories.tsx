import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { Text, TextSize, TextTheme } from "./Text";

import "app/styles/index.scss";

const meta = {
    title: "shared/Text",
    component: Text,
    tags: ["autodocs"],
} satisfies Meta<typeof Text>;

export default meta;

type Story = StoryObj<typeof meta>;

export const TextDefault: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
    },
};

export const TextOnlyTitle: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
    },
};

export const TextOnlyText: Story = {
    args: {
        text: "Текст о чём-то очень важном",
    },
};

export const TextDefaultDARK: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const TextOnlyTitleDARK: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const TextOnlyTextDARK: Story = {
    args: {
        text: "Текст о чём-то очень важном",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const TextErrorDARK: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
        styledComponentTheme: TextTheme.ERROR,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const TextPrimaryDARK: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
        styledComponentTheme: TextTheme.PRIMARY,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const SizeM: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
        styledComponentTheme: TextTheme.PRIMARY,
        size: TextSize.M,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const SizeL: Story = {
    args: {
        title: "Заголовок о чём-то очень важном",
        text: "Текст о чём-то очень важном",
        styledComponentTheme: TextTheme.PRIMARY,
        size: TextSize.L,
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};
