import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";

import { Loader } from "./Loader";
import { ThemeDecorator } from "../../config/storybook/ThemeDecorator/ThemeDecorator";

import "app/styles/index.scss";

const meta = {
    title: "widgets/Loader",
    component: Loader,
    tags: ["autodocs"],
} satisfies Meta<typeof Loader>;

export default meta;

type Story = StoryObj<typeof meta>;

export const LoaderLight: Story = {
    args: {},
};

export const LoaderDark: Story = {
    args: {},
    decorators: [ThemeDecorator(Theme.DARK)],
};
