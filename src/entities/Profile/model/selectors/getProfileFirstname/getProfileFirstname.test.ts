import { StateSchema } from "app/providers/StoreProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getProfileFirstname } from "./getProfileFirstname";

describe("getProfileFirstname", () => {
    test("should return Firstname", () => {
        const data = {
            username: "admin",
            age: 22,
            country: Country.Armenia,
            lastname: "borisvch",
            first: "ruslan",
            city: "acs",
            currency: Currency.EUR,
        };
        const state: DeepPartial<StateSchema> = {
            profile: {
                data,
            },
        };
        expect(getProfileFirstname(state as StateSchema)).toEqual("ruslan");
    });
    test("should empty", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getProfileFirstname(state as StateSchema)).toEqual("");
    });
});
