import { FC } from "react";
import { ErrorBoundary } from "react-error-boundary";

import { ErrorPage } from "widgets/ErrorPage/ui/ErrorPage";
import { classNames } from "shared/lib/classNames/classNames";

import styles from "./ErrorBoundaryProvider.module.scss";

export interface ErrorBoundaryProviderProps {
    children: React.ReactNode;
    className?: string;
}

interface ErrorProps {
    error: Error;
    resetErrorBoundary: () => void;
}

const Fallback: FC<ErrorProps> = ({ error, resetErrorBoundary }) => (
    // Call resetErrorBoundary() to reset the error boundary and retry the render.
    <div className={styles.ErrorBoundaryProvider}>
        <ErrorPage error={error} resetErrorBoundary={resetErrorBoundary} />
    </div>
);
export const ErrorBoundaryProvider: FC<ErrorBoundaryProviderProps> = ({
    children,
    className,
}) => (
    <div className={classNames("", {}, [className])}>
        <ErrorBoundary
            FallbackComponent={Fallback}
            onReset={(details) => {
                // Reset the state of your app so the error doesn't happen again
            }}
        >
            {children}
        </ErrorBoundary>
    </div>
);
