import { memo } from "react";
import { styled } from "@mui/material";

import { Text, TextAlign } from "shared/ui/Text/Text";

import { ArticleImageBlock } from "../../model/types/article";

interface ArticleImageBlockComponentProps {
    block: ArticleImageBlock;
}

const StyledArticleImageBlockComponent = styled("div")({});

const StyledImage = styled("img")({
    maxWidth: "100%",
});

export const ArticleImageBlockComponent = memo(
    (props: ArticleImageBlockComponentProps) => {
        const { block } = props;
        return (
            <StyledArticleImageBlockComponent {...props}>
                <StyledImage src={block.src} alt={block.title} />
                {block.title && (
                    <Text text={block.title} align={TextAlign.CENTER} />
                )}
            </StyledArticleImageBlockComponent>
        );
    },
);
