import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";

import {
    getProfileReadonly,
    profileActions,
    updateProfileData,
} from "entities/Profile";
import { useAppDispatch } from "shared/lib/hooks/useAppDispatch/useAppDispatch";
import { Button, ButtonTheme } from "shared/ui/Button/Button";
import { Text } from "shared/ui/Text/Text";

interface ProfilePageHeaderProps {}

const StyledProfilePageHeader = styled.div``;

const StyledHeader = styled.div`
    display: flex;
    margin-bottom: 20px;
`;

const StyledButtonCancel = styled(Button)`
    margin-left: auto;
    margin-right: 20px;
`;

const StyledButtonEdit = styled(Button)`
    margin-left: auto;
`;

const StyledButtonSave = styled(Button)``;

export const ProfilePageHeader = (props: ProfilePageHeaderProps) => {
    const readonly = useSelector(getProfileReadonly);
    const { t } = useTranslation("profile");
    const dispatch = useAppDispatch();

    const onEdit = useCallback(() => {
        dispatch(profileActions.setReadonly(false));
    }, [dispatch]);

    const onCancelEdit = useCallback(() => {
        dispatch(profileActions.cancelEdit());
    }, [dispatch]);

    const onSave = useCallback(() => {
        // @ts-ignore
        dispatch(updateProfileData());
    }, [dispatch]);

    return (
        <StyledProfilePageHeader>
            <StyledHeader>
                <Text title={t("Профиль")} />
                {readonly ? (
                    <StyledButtonEdit
                        styledComponentTheme={ButtonTheme.OUTLINE}
                        onClick={onEdit}
                    >
                        {t("Редактировать")}
                    </StyledButtonEdit>
                ) : (
                    <>
                        <StyledButtonCancel
                            styledComponentTheme={ButtonTheme.OUTLINE_RED}
                            onClick={onCancelEdit}
                        >
                            {t("Отменить")}
                        </StyledButtonCancel>
                        <StyledButtonSave
                            styledComponentTheme={ButtonTheme.OUTLINE}
                            onClick={onSave}
                        >
                            {t("Сохранить")}
                        </StyledButtonSave>
                    </>
                )}
            </StyledHeader>
        </StyledProfilePageHeader>
    );
};
