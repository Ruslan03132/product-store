import { FC, useEffect, useState } from "react";

import { classNames } from "shared/lib/classNames/classNames";
import { Button, ButtonTheme } from "shared/ui/Button/Button";

import styles from "./BugButton.module.scss";

interface BugButtonProps {
    className?: string;
}

export const BugButton: FC<BugButtonProps> = ({ className }) => {
    const [value, setValue] = useState(0);

    const clickBug = () => {
        setValue(Math.random());
    };

    useEffect(() => {
        if (value > 0.5) {
            throw new Error();
        }
    }, [value]);

    return (
        <div className={classNames(styles.BugButton, {}, [className])}>
            <Button onClick={clickBug} theme={ButtonTheme.BACKGROUND}>BUG</Button>
        </div>
    );
};
