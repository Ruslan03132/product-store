import { ButtonHTMLAttributes, memo, ReactNode } from "react";

import { classNames, Mods } from "shared/lib/classNames/classNames";

import styles from "./Button.module.scss";

export enum ButtonTheme {
    CLEAR = "clear",
    CLEAR_INVERTED = "clearInverted",
    OUTLINE = "outline",
    OUTLINE_RED = "outline_red",
    BACKGROUND = "background",
    BACKGROUND_INVERTED = "backgroundInverted",
}

export enum ButtonSize {
    M = "size_m",
    L = "size_l",
    XL = "size_xl",
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: ReactNode;
    className?: string;
    theme?: ButtonTheme;
    styledComponentTheme?: ButtonTheme;
    square?: boolean;
    size?: ButtonSize;
    disabled?: boolean;
}

export const Button = memo(
    ({
        children,
        className,
        theme,
        styledComponentTheme,
        square,
        size = ButtonSize.M,
        disabled,
        ...otherProps
    }: ButtonProps) => {
        const mods: Mods = {
            [styles.square]: square,
            [styles.disabled]: disabled,
        };

        return (
            <button
                type="button"
                className={classNames(styles.Button, mods, [
                    styles[theme || ""],
                    className,
                    styles[size],
                    styles[styledComponentTheme || ""],
                ])}
                disabled={disabled}
                {...otherProps}
            >
                {children}
            </button>
        );
    },
);
