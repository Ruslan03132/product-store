import { PaletteColor, ThemeOptions } from "@mui/material";
import { Theme } from "@mui/material/styles";

declare module "@mui/material/styles" {
    interface Palette {
        invertedPrimary: string;
        invertedSecondary: string;
        bgColor: string;
        invertedBgColor: string;
        skeletonColor?: string;
        skeletonShadow?: string;
        codeBg?: string;
    }

    interface Theme {
        global: {
            redLight: string;
        };
    }

    interface ThemeOptions {
        global: {
            redLight: string;
        };
    }

    interface PaletteOptions {
        invertedPrimary: string;
        invertedSecondary: string;
        bgColor: string;
        invertedBgColor: string;
        skeletonColor?: string;
        skeletonShadow?: string;
        codeBg?: string;
    }
}
