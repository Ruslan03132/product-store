import { Country } from "entities/Country";
import { Currency } from "entities/Currency";

import { profileActions, profileReducer } from "./profileSlice";
import { DeepPartial } from "../../../../shared/lib/tests/deepPartial";
import { updateProfileData } from "../services/updateProfileData/updateProfileData";
import { Profile, ProfileSchema, ValidateProfileError } from "../types/profile";

describe("profileSlice", () => {
    test("setReadonly", () => {
        const state: DeepPartial<ProfileSchema> = {};
        expect(
            profileReducer(
                state as ProfileSchema,
                profileActions.setReadonly(true),
            ),
        ).toEqual({ readonly: true });
    });

    test("test cancel edit", () => {
        const state: DeepPartial<ProfileSchema> = {
            data: {
                first: "Lena",
                lastname: "Podolyaka",
            },
        };
        expect(
            profileReducer(state as ProfileSchema, profileActions.cancelEdit()),
        ).toEqual({
            ...state,
            form: state.data,
            readonly: true,
            validateErrors: undefined,
        });
    });

    test("updateProfile", () => {
        const state: DeepPartial<ProfileSchema> = {
            form: {
                first: "Lena",
                lastname: "Podolyaka",
            },
        };
        expect(
            profileReducer(
                state as ProfileSchema,
                profileActions.updateProfile({
                    first: "Andrey",
                    lastname: "Dubov",
                }),
            ),
        ).toEqual({
            form: {
                first: "Andrey",
                lastname: "Dubov",
            },
        });
    });

    test("updateProfileData pending", () => {
        const state: DeepPartial<ProfileSchema> = {
            isLoading: false,
            validateErrors: [ValidateProfileError.SERVER_ERROR],
        };
        expect(
            // @ts-ignore
            profileReducer(state as ProfileSchema, updateProfileData.pending),
        ).toEqual({ isLoading: true, validateErrors: undefined });
    });

    test("updateProfileData fulfilled", () => {
        const state: DeepPartial<ProfileSchema> = {
            isLoading: true,
            validateErrors: [ValidateProfileError.SERVER_ERROR],
        };
        const data = {
            username: "Admin",
            age: 27,
            country: Country.Armenia,
            lastname: "Cooper",
            first: "Alice",
            city: "acs",
            currency: Currency.RUB,
        };

        expect(
            profileReducer(
                state as ProfileSchema,
                // @ts-ignore
                updateProfileData.fulfilled(data),
            ),
        ).toEqual({
            isLoading: false,
            data,
            form: data,
            readonly: true,
            validateErrors: undefined,
        });
    });
});
