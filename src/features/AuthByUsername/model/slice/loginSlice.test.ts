import { loginActions, loginReducer } from "./loginSlice";
import { DeepPartial } from "../../../../shared/lib/tests/deepPartial";
import {
    loginByUsername,
    LoginByUsernameProps,
} from "../services/loginByUsername/loginByUsername";
import { LoginSchema } from "../types/loginSchema";

describe("loginSlice", () => {
    test("setUsername", () => {
        const state: DeepPartial<LoginSchema> = {
            username: "123",
        };
        expect(
            loginReducer(state as LoginSchema, loginActions.setUsername("1")),
        ).toEqual({ username: "1" });
    });
    test("setPassword", () => {
        const state: DeepPartial<LoginSchema> = {
            password: "abc",
        };
        expect(
            loginReducer(state as LoginSchema, loginActions.setPassword("acb")),
        ).toEqual({ password: "acb" });
    });
    // test("setError", () => {
    //     const state: DeepPartial<LoginSchema> = {
    //         error: "error",
    //     };
    //     expect(
    //         loginReducer(
    //             state as LoginSchema,
    //             loginByUsername.rejected(new Error("newerror"), "login/loginByUsername", {
    //                 username: "123",
    //                 password: "123",
    //             }),
    //         ),
    //     ).toEqual({ error: "newerror", isLoading: false });
    // });
});
