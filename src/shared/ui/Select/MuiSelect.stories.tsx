import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { MuiSelect } from "./MuiSelect";

const meta = {
    title: "shared/MuiSelect",
    component: MuiSelect,
    tags: ["autodocs"],
} satisfies Meta<typeof MuiSelect>;

export default meta;

type Story = StoryObj<typeof meta>;

export const PrimaryModal: Story = {
    args: { label: "Укажите значение" },
};

export const PrimaryModalDark: Story = {
    args: {
        label: "Укажите значение",
        options: [
            { value: "123", content: "Первый пункт" },
            { value: "1234", content: "Второй пункт" },
        ],
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};
