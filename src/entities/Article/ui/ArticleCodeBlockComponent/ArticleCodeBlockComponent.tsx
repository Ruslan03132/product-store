import { memo } from "react";
import { styled } from "@mui/material";

import { Code } from "shared/ui/Code/Code";

import { ArticleCodeBlock } from "../../model/types/article";

interface ArticleCodeBlockComponentProps {
    block: ArticleCodeBlock;
}

const StyledArticleCodeBlockComponent = styled("div")({
    width: "100%",
});

export const ArticleCodeBlockComponent = memo(
    (props: ArticleCodeBlockComponentProps) => {
        const { block } = props;
        return (
            <StyledArticleCodeBlockComponent {...props}>
                <Code text={block.code} />
            </StyledArticleCodeBlockComponent>
        );
    },
);
