import { StateSchema } from "app/providers/StoreProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getProfileForm } from "./getProfileForm";

describe("getProfileForm", () => {
    test("should return Form", () => {
        const form = {
            username: "admin",
            age: 22,
            country: Country.Armenia,
            lastname: "borisvch",
            first: "ruslan",
            city: "acs",
            currency: Currency.EUR,
        };
        const state: DeepPartial<StateSchema> = {
            profile: {
                form,
            },
        };
        expect(getProfileForm(state as StateSchema)).toEqual(form);
    });
    test("should undefined", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getProfileForm(state as StateSchema)).toEqual(undefined);
    });
});
