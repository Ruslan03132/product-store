import type { Meta, StoryObj } from "@storybook/react";

import { StoreDecorator } from "shared/config/storybook/StoreDecorator/StoreDecorator";

import LoginForm from "./LoginForm";

import "app/styles/index.scss";

const meta = {
    title: "features/LoginForm",
    component: LoginForm,
    tags: ["autodocs"],
} satisfies Meta<typeof LoginForm>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Form: Story = {
    args: {},
    decorators: [
        StoreDecorator({ loginForm: { username: "123", password: "asd" } }),
    ],
};

export const FormWithError: Story = {
    args: {},
    decorators: [
        StoreDecorator({
            loginForm: { username: "123", password: "asd", error: "Error" },
        }),
    ],
};

export const FormWithLoading: Story = {
    args: {},
    decorators: [
        StoreDecorator({
            loginForm: { username: "123", password: "asd", isLoading: true },
        }),
    ],
};
