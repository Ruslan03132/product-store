import { ReactNode, useEffect } from "react";
import { useDispatch, useStore } from "react-redux";
import { Reducer } from "@reduxjs/toolkit";
import styled from "styled-components";

import { ReduxStoreWithManager } from "app/providers/StoreProvider";
import { StateSchemaKey } from "app/providers/StoreProvider/config/StateSchema";

export type ReducersList = {
    [name in StateSchemaKey]?: Reducer;
};

type ReducersListEntry = [StateSchemaKey, Reducer];

interface DynamicModuleLoaderProps {
    children: ReactNode;
    reducers: ReducersList;
    removeAfterUnmount?: boolean;
}

const StyledDynamicModuleLoader = styled.div``;

export const DynamicModuleLoader = (props: DynamicModuleLoaderProps) => {
    const { children, reducers, removeAfterUnmount } = props;
    const store = useStore() as ReduxStoreWithManager;
    const dispatch = useDispatch();
    useEffect(() => {
        Object.entries(reducers).forEach(
            ([name, reducer]) => {
                store.reducerManager.add(name as StateSchemaKey, reducer);
                dispatch({
                    type: `>>>>>>>>INIT ${name}  REDUCER!!!!`,
                });
            },
        );
        return () => {
            if (removeAfterUnmount) {
                Object.entries(reducers).forEach(
                    ([name, reducer]) => {
                        store.reducerManager.remove(name as StateSchemaKey);
                        dispatch({
                            type: `>>>>>>>>REMOVE ${name}  REDUCER!!!!`,
                        });
                    },
                );
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return <StyledDynamicModuleLoader>{children}</StyledDynamicModuleLoader>;
};
