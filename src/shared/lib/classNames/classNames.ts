export type Mods = Record<string, boolean | undefined | null>;

export function classNames(
    cls: string,
    mods: Mods = {},
    additional: Array<string | undefined> = [],
): string {
    return [
        cls,
        ...additional.filter(Boolean),
        ...Object.entries(mods).reduce((acc: string[], [cls, marker]) => {
            if (marker) {
                acc.push(cls);
            }
            return acc;
        }, []),
    ].join(" ");
}
