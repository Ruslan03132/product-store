import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { styled } from "@mui/material";

import { ArticleDetails } from "entities/Article";
import { Skeleton } from "shared/ui/Skeleton/Skeleton";

interface ArticleDetailsPageProps {}

const StyledArticleDetailsPage = styled("div")({});

const ArticleDetailsPage = (props: ArticleDetailsPageProps) => {
    const { t } = useTranslation("article");

    const { id } = useParams<{ id: string }>();

    if (!id) {
        return (
            <StyledArticleDetailsPage {...props}>
                {t("Статья не найдена")}
            </StyledArticleDetailsPage>
        );
    }
    return (
        <StyledArticleDetailsPage {...props}>
            {t("Страница статьи")}
            <ArticleDetails id={id} />
        </StyledArticleDetailsPage>
    );
};

export default ArticleDetailsPage;
