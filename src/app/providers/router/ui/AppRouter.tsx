import { memo, ReactNode, Suspense, useCallback, useMemo } from "react";
import { useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import path from "path";

import {
    AppRoutes,
    AppRoutesProps,
    routeConfig,
} from "app/config/routeConfig/routeConfig";
import { PageLoader } from "widgets/PageLoader";
import { getUserAuthData } from "entities/User";

import { RequireAuth } from "./RequireAuth";

const AppRouter = () => {
    const renderWithWrapper = useCallback((route: AppRoutesProps) => {
        const elem = route.element;
        return (
            <Route
                element={
                    route.authOnly ? (
                        <RequireAuth>{elem}</RequireAuth>
                    ) : (
                        // @ts-ignore
                        elem
                    )
                }
                path={route.path}
                key={route.path}
            />
        );
    }, []);

    return (
        <Suspense fallback={<PageLoader />}>
            <Routes>{Object.values(routeConfig).map(renderWithWrapper)}</Routes>
        </Suspense>
    );
};

export default memo(AppRouter);
