import { FC, lazy } from "react";

import { LoginFormProps } from "./LoginForm";

export const LoginFormAsync = lazy<FC<LoginFormProps>>(
    () =>
        // eslint-disable-next-line implicit-arrow-linebreak
        new Promise((res) => {
            // @ts-ignore
            setTimeout(() => res(import("./LoginForm")), 1500);
        }),
);
