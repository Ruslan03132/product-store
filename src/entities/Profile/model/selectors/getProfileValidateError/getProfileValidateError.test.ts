import { StateSchema } from "app/providers/StoreProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getProfileValidateError } from "./getProfileValidateError";
import { ValidateProfileError } from "../../types/profile";

describe("getProfileValidateError", () => {
    test("should return error", () => {
        const validateError = [
            ValidateProfileError.INCORRECT_AGE,
            ValidateProfileError.INCORRECT_USER_DATA,
            ValidateProfileError.NO_CITY,
        ];
        const state: DeepPartial<StateSchema> = {
            profile: {
                validateErrors: validateError,
            },
        };
        expect(getProfileValidateError(state as StateSchema)).toEqual(
            validateError,
        );
    });
    test("should null", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getProfileValidateError(state as StateSchema)).toEqual(null);
    });
});
