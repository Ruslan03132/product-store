import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import App from "app/App";
import { ErrorBoundaryProvider } from "app/providers/ErrorBoundaryProvider";
import { MuiThemeProvider } from "app/providers/MuiThemeProvider/MuiThemeProvider";
import { StoreProvider } from "app/providers/StoreProvider";
import { ThemeProvider } from "app/providers/ThemeProvider";

import "shared/config/i18n/i18n";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement,
);

root.render(
    <BrowserRouter>
        <StoreProvider>
            <ErrorBoundaryProvider>
                <ThemeProvider>
                    <MuiThemeProvider>
                        <App />
                    </MuiThemeProvider>
                </ThemeProvider>
            </ErrorBoundaryProvider>
        </StoreProvider>
    </BrowserRouter>,
);
