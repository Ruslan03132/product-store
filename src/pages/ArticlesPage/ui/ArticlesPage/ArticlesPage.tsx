import { memo } from "react";
import { useTranslation } from "react-i18next";
import { styled } from "@mui/material";

interface ArticlesPageProps {}

const StyledArticlesPage = styled("div")({
    minHeight: "100%",
});

const ArticlesPage = (props: ArticlesPageProps) => {
    const { t } = useTranslation("article");

    return (
        <StyledArticlesPage {...props}>
            {t("Страница статей")}
        </StyledArticlesPage>
    );
};

export default memo(ArticlesPage);
