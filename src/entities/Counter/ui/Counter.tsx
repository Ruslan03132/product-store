import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Button } from "shared/ui/Button/Button";

import { getCounterValue } from "../model/selectors/getCounterValue/getCounterValue";
import { counterActions } from "../model/slice/conterSlice";

export const Counter: FC = (props) => {
    const dispatch = useDispatch();
    const counterValue = useSelector(getCounterValue);
    const increment = () => {
        dispatch(counterActions.increment());
    };

    const decrement = () => {
        dispatch(counterActions.decrement());
    };
    return (
        <div>
            <Button onClick={increment} data-testid="increment-btn">
                increment
            </Button>
            VALUE:
            <h1 data-testid="value-title">{counterValue}</h1>
            <Button onClick={decrement} data-testid="decrement-btn">
                decrement
            </Button>
        </div>
    );
};
