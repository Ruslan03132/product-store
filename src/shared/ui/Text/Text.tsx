/* eslint-disable indent */
/* eslint-disable consistent-return */
import { FC, memo } from "react";
import { css, styled } from "styled-components";

export enum TextTheme {
    PRIMARY = "primary",
    ERROR = "error",
}

export enum TextAlign {
    RIGHT = "right",
    LEFT = "left",
    CENTER = "center",
}

export enum TextSize {
    M = "size_m",
    L = "size_l",
}

interface TextProps {
    title?: string;
    text?: string;
    styledComponentTheme?: TextTheme;
    align?: TextAlign;
    size?: TextSize;
}

const StyledTextWrapper = styled.div<{ align: TextAlign }>`
    text-align: ${(props) => {
        switch (props.align) {
            case "right":
                return css`
                    right;
                `;
            case "left":
                return css`
                    left;
                `;
            case "center":
                return css`
                    center;
                `;
            default:
                return css`
                    center;
                `;
        }
    }};
`;

const StyledTitle = styled.p<TextProps>`
    font: ${(props) => {
        switch (props.size) {
            case TextSize.M:
                return css`var(--font-l)`;
            case TextSize.L:
                return css`var(--font-xl)`;
            default:
                return css`var(--font-m)`;
        }
    }};

    color: ${(props) => {
        switch (props.styledComponentTheme) {
            case "primary":
                return css`var(--primary-color)`;
            case "error":
                return css`var(--red-ligth)`;
            default:
                return css`var(--primary-color)`;
        }
    }};
`;

const StyledText = styled.p<TextProps>`
    font: ${(props) => {
        switch (props.size) {
            case TextSize.M:
                return css`var(--font-m)`;
            case TextSize.L:
                return css`var(--font-l)`;
            default:
                return css`var(--font-m)`;
        }
    }};
    color: ${(props) => {
        switch (props.styledComponentTheme) {
            case "primary":
                return css`var(--secondary-color)`;
            case "error":
                return css`var(--red-dark)`;
            default:
                return css`var(--secondary-color)`;
        }
    }};
`;

export const Text = memo((props: TextProps) => {
    const {
        title,
        text,
        styledComponentTheme = TextTheme.PRIMARY,
        align = TextAlign.LEFT,
        size = TextSize.M,
        ...otherProps
    } = props;
    return (
        <StyledTextWrapper align={align} {...otherProps}>
            {title && (
                <StyledTitle
                    styledComponentTheme={styledComponentTheme}
                    size={size}
                >
                    {title}
                </StyledTitle>
            )}
            {text && (
                <StyledText
                    styledComponentTheme={styledComponentTheme}
                    size={size}
                >
                    {text}
                </StyledText>
            )}
        </StyledTextWrapper>
    );
});
