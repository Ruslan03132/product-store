import { StateSchema } from "app/providers/StoreProvider";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getLoginPassword } from "./getLoginPassword";

describe("getLoginPassword", () => {
    test("should return password", () => {
        const state: DeepPartial<StateSchema> = {
            loginForm: {
                password: "adwad",
            },
        };
        expect(getLoginPassword(state as StateSchema)).toEqual("adwad");
    });
    test("should empty", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getLoginPassword(state as StateSchema)).toEqual("");
    });
});
