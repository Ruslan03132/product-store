import { useMemo } from "react";
import { styled } from "@mui/material";
import { default as Avt } from "@mui/material/Avatar";

interface AvatarProps {
    url?: string;
    avatarSize?: number;
    alt?: string;
}

const StyledAvatar = styled("div")({});

export const Avatar = (props: AvatarProps) => {
    const { url, avatarSize, alt, ...otherprops } = props;
    const sizeProps = useMemo(
        () => ({
            width: avatarSize || 100,
            height: avatarSize || 100,
        }),
        [avatarSize],
    );
    return (
        <StyledAvatar {...otherprops}>
            <Avt alt={alt} src={url} sx={sizeProps} />
        </StyledAvatar>
    );
};
