import webpack from "webpack";

import { BuildOptions } from "./types/config";

export function buildResolvers(options: BuildOptions): webpack.ResolveOptions {
    return {
        extensions: [".tsx", ".ts", ".js", ".scss", ".svg"],
        // preferAbsolute: true,
        // разрешенные пути для абсолютного импорта
        // mainFields: ["index"]
        alias: { "@mui/styled-engine": "@mui/styled-engine-sc" },
        modules: [options.paths.src, "node_modules"],
    };
}
