# product-store

1) Главная страница (/):
![main](/uploads/2ec78f2f7a959bc2f576076f7bb7294f/main.jpg)
2) О сайте (/about): 
![about](/uploads/6b7978abe4dfcb97145e8b42303adbac/about.jpg)
3) Страница профиля пользователя (/profile):
![profile](/uploads/0a7e95466fd709e457cb3ef7c693b17f/profile.jpg)
4) Страница статей (/articles):
![articles](/uploads/2297caf0f2788ff1dd6812bdf75049b2/articles.jpg)
5) Страница статьи (/articles/1):
![articles1](/uploads/554bcab8a11c916b56a185e1ad0b16f5/articles1.jpg)
![articles1_2](/uploads/ab07f40af4b104379cc4d140410c4d24/articles1_2.jpg)
6) Страница загрузки статьи (/articles/1):
![articleIsLoading](/uploads/60511645d2816d36e9b3032458baf7ee/articleIsLoading.jpg)
7) Аутентификация (с любой доступной страницы)
![auth](/uploads/fdddefd41a935356816b91b9ec835e8e/auth.jpg)
8) Свернутый сайдбар вместе с лоадером
![shortsidebar_with_loader](/uploads/05e6001bdcff9706a3262e02e53073bb/shortsidebar_with_loader.jpg)