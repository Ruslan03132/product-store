import { StateSchema } from "app/providers/StoreProvider";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getLoginUsername } from "./getLoginUsername";

describe("getLoginUsername", () => {
    test("should return username", () => {
        const state: DeepPartial<StateSchema> = {
            loginForm: {
                username: "azalo123",
            },
        };
        expect(getLoginUsername(state as StateSchema)).toEqual("azalo123");
    });
    test("should empty", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getLoginUsername(state as StateSchema)).toEqual("");
    });
});
