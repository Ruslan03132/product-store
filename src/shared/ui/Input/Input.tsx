import { InputHTMLAttributes, memo, useEffect, useRef, useState } from "react";
import styled, { keyframes } from "styled-components";

const InputWrapper = styled.div`
    width: 500px;
    display: flex;
`;

const StyledPlaceHolder = styled.div`
    margin-right: 5px;
`;

const StyledInput = styled.input<{ readonly?: boolean }>`
    background-color: transparent;
    border: none;
    outline: none;
    caret-color: transparent;
    color: var(--primary-color);
    font: var(--font-m);
    opacity: ${(props) => (props.readonly ? 0.7 : 1)};
`;

const CaretWrapper = styled.div`
    flex-grow: 1;
    position: relative;
`;

const animationCaret = keyframes`
    0%{
        opacity: 0;
    }

    50% {
        opacity: 0.001;
    }

    100% {
        opacity: 1;
    }
`;

type CaretProps = {
    caretposition: number;
};

const Caret = styled.span<CaretProps>`
    position: absolute;
    height: 3px;
    width: 9px;
    background: var(--primary-color);
    bottom: 0;
    left: ${(props) => `${props.caretposition * 9}px`};
    animation-name: ${animationCaret};
    animation-duration: 0.5s;
    animation-iteration-count: infinite;
`;

type HTMLInputProps = Omit<
    InputHTMLAttributes<HTMLInputElement>,
    "values" | "onChange"
>;
interface InputProps extends HTMLInputProps {
    value?: string | number;
    onChange?: (value: string) => void;
    readonly?: boolean;
}

export const Input = memo((props: InputProps) => {
    const { value, onChange, placeholder, autoFocus, readonly, ...otherProps } =
        props;
    const ref = useRef<HTMLInputElement>(null);
    const [isFocused, setIsFocused] = useState(false);
    const [caretPosition, setCaretPosition] = useState<number>(0);
    const isCaretVisible = isFocused && !readonly;
    useEffect(() => {
        if (autoFocus) {
            setIsFocused(true);
            ref.current?.focus();
        }
    }, [autoFocus]);

    const onFocus = () => {
        setIsFocused(true);
    };

    const onBlur = () => {
        setIsFocused(false);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const onSelect = (e: any) => {
        if (e.target.value.length >= 21) return;
        setCaretPosition(e.target.selectionStart || 0);
    };

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (caretPosition >= 21) {
            onChange?.(e.target.value);
            return;
        }
        onChange?.(e.target.value);
        setCaretPosition(e.target.value.length);
    };

    return (
        <InputWrapper {...otherProps}>
            {placeholder && (
                // eslint-disable-next-line no-useless-concat
                <StyledPlaceHolder>{`${placeholder}` + ">"}</StyledPlaceHolder>
            )}
            <CaretWrapper>
                <StyledInput
                    ref={ref}
                    value={value}
                    onChange={onChangeHandler}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onSelect={onSelect}
                    disabled={readonly}
                    readonly={readonly}
                />
                {isCaretVisible && <Caret caretposition={caretPosition} />}
            </CaretWrapper>
        </InputWrapper>
    );
});
