import { fireEvent, screen } from "@testing-library/react";

import { RenderComponent } from "shared/lib/tests/renderComponent/renderComponent";

import "shared/config/i18n/i18n";
import "jest-styled-components";

import { Sidebar } from "./Sidebar";

describe("Sidebar", () => {
    test("sd1", () => {
        RenderComponent(<Sidebar />);
    });

    test("testtoogle", () => {
        RenderComponent(<Sidebar />);
        const tglBtn = screen.getByTestId("tglbtn");
        const sidebar = screen.getByTestId("sidebar");
        expect(sidebar).toBeInTheDocument();
        fireEvent.click(tglBtn);
        expect(sidebar).toHaveClass("collapsed");
    });
});
