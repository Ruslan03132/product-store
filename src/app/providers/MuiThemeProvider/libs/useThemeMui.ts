import { Dispatch, SetStateAction, useState } from "react";

import { Theme, useTheme } from "app/providers/ThemeProvider";
import { darkTheme, lightTheme } from "app/styles/mui-styles/createTheme";

export const useThemeMui = () => {
    const [themeMUI, setThemeMUI] = useState(lightTheme);
    return { themeMUI, setThemeMUI };
};
