import { StoryFn } from "@storybook/react";

import { MuiThemeProvider } from "app/providers/MuiThemeProvider/MuiThemeProvider";

export const MuiDecorator = (Story: StoryFn) => (
    <MuiThemeProvider>
        <Story />
    </MuiThemeProvider>
);
