import { useTranslation } from "react-i18next";
import styled, { css } from "styled-components";

import { CountrySelect } from "entities/Country";
import { Country } from "entities/Country/model/types/country";
import { CurrencySelect } from "entities/Currency";
import { Currency } from "entities/Currency/model/types/currency";
import { Profile } from "entities/Profile/model/types/profile";
import { Avatar } from "shared/ui/Avatar/Avatar";
import { Input } from "shared/ui/Input/Input";
import { Loader } from "shared/ui/Loader/Loader";
import { MuiSelect } from "shared/ui/Select/MuiSelect";
import { Text, TextAlign, TextTheme } from "shared/ui/Text/Text";

interface ProfileCardProps {
    data?: Profile;
    isLoading?: boolean;
    error?: string;
    onChangeFirstname?: (value?: string) => void;
    onChangeLastname?: (value?: string) => void;
    onChangeCity?: (value?: string) => void;
    onChangeAge?: (value?: string) => void;
    onChangeUsername?: (value?: string) => void;
    onChangeAvatar?: (value?: string) => void;
    onChangeCurrency?: (currency: Currency) => void;
    onChangeCountry?: (currency: Country) => void;
    readonly?: boolean;
}

const StyledProfileCard = styled.div<{ readonly: boolean }>`
    padding: 20px;
    border: 2px solid
        ${(props) =>
        (props.readonly
            ? css`var(--inverted-bg-color)`
            : css`var(--inverted-primary-color)`)};
    display: flex;
    align-items: center;
    justify-content: center;
`;

const StyledData = styled.div``;

const StyledInput = styled(Input)`
    margin-top: 10px;
`;

const StyledAvatar = styled(Avatar)`
    display: flex;
    width: 100%;
    justify-content: center;
    opacity: 0.9;
`;

export const ProfileCard = (props: ProfileCardProps) => {
    const { t } = useTranslation("profile");
    const {
        data,
        isLoading,
        error,
        onChangeFirstname,
        onChangeLastname,
        onChangeCity,
        onChangeAge,
        onChangeUsername,
        onChangeAvatar,
        onChangeCurrency,
        onChangeCountry,
        readonly = true,
    } = props;

    if (isLoading) {
        return (
            <StyledProfileCard readonly>
                <Loader />
            </StyledProfileCard>
        );
    }

    if (error) {
        return (
            <StyledProfileCard readonly>
                <Text
                    styledComponentTheme={TextTheme.ERROR}
                    title={t("Произошла ошибка при загрузке профиля")}
                    text={t("Попробуйте обновить страницу")}
                    align={TextAlign.CENTER}
                />
            </StyledProfileCard>
        );
    }

    return (
        <StyledProfileCard readonly={readonly}>
            <StyledData>
                {data?.avatar && <StyledAvatar url={data?.avatar} />}
                <StyledInput
                    value={data?.first}
                    placeholder={t("Ваше имя")}
                    onChange={onChangeFirstname}
                    readonly={readonly}
                />
                <StyledInput
                    value={data?.lastname}
                    placeholder={t("Вашa фамилия")}
                    onChange={onChangeLastname}
                    readonly={readonly}
                />
                <StyledInput
                    value={data?.age}
                    placeholder={t("Ваш возраст")}
                    onChange={onChangeAge}
                    readonly={readonly}
                />
                <StyledInput
                    value={data?.city}
                    placeholder={t("Ваш город")}
                    onChange={onChangeCity}
                    readonly={readonly}
                />
                <StyledInput
                    value={data?.username}
                    placeholder={t("Введите имя пользователя")}
                    onChange={onChangeUsername}
                    readonly={readonly}
                />
                <StyledInput
                    value={data?.avatar}
                    placeholder={t("Введите ссылку на аватар")}
                    onChange={onChangeAvatar}
                    readonly={readonly}
                />
                <CurrencySelect
                    value={data?.currency}
                    onChange={onChangeCurrency}
                    readonly={readonly}
                />
                <CountrySelect
                    value={data?.country}
                    onChange={onChangeCountry}
                    readonly={readonly}
                />
            </StyledData>
        </StyledProfileCard>
    );
};
