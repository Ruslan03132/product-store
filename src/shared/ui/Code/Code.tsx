import { memo, ReactNode, useCallback } from "react";
import { styled } from "@mui/material";

import CopyIcon from "shared/assets/icons/copy-20-20.svg";

import { Button, ButtonTheme } from "../Button/Button";
import { Icon } from "../Icon/Icon";

interface CodeProps {
    text: string;
}

const StyledCopyBtn = styled(Button)(({ theme }) => ({
    position: "absolute",
    right: "20px",
    top: "20px",
    background: theme.palette.codeBg,
}));

const StyledCode = styled("pre")(({ theme }) => ({
    border: `1px solid ${theme.palette.primary.main}`,
    padding: "20px",
    position: "relative",
    overflow: "auto",
}));

const StyledCopyIcon = styled(CopyIcon)(({ theme }) => ({
    "& path": {
        stroke: theme.palette.primary.main,
    },
}));

export const Code = memo((props: CodeProps) => {
    const { text } = props;

    const onCopy = useCallback(() => {
        navigator.clipboard.writeText(text);
    }, [text]);
    return (
        <StyledCode {...props}>
            <StyledCopyBtn
                onClick={onCopy}
                styledComponentTheme={ButtonTheme.CLEAR}
            >
                <StyledCopyIcon />
            </StyledCopyBtn>
            <code>{text}</code>
        </StyledCode>
    );
});
