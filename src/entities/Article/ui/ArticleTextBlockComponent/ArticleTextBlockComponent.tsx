import { memo } from "react";
import { styled } from "@mui/material";

import { Text } from "shared/ui/Text/Text";

import { ArticleTextBlock } from "../../model/types/article";

interface ArticleTextBlockComponentProps {
    block: ArticleTextBlock;
}

const StyledArticleTextBlockComponent = styled("div")({});

const StyledArticleText = styled(Text)({
    marginBottom: "16px",
});

const StyledParagraph = styled(Text)({ marginBottom: "8px" });

export const ArticleTextBlockComponent = memo(
    (props: ArticleTextBlockComponentProps) => {
        const { block } = props;
        return (
            <StyledArticleTextBlockComponent {...props}>
                {block.title && <StyledArticleText title={block.title} />}
                {block.paragraphs.map((paragraph, index) => (
                    <StyledParagraph key={paragraph} text={paragraph} />
                ))}
            </StyledArticleTextBlockComponent>
        );
    },
);
