import { ReactNode, useEffect } from "react";
import { ThemeProvider } from "@mui/material/styles";

import {
    darkTheme,
    lightTheme,
    orangeTheme,
} from "app/styles/mui-styles/createTheme";

import { useThemeMui } from "./libs/useThemeMui";
import { Theme, useTheme } from "../ThemeProvider";

interface MuiThemeProviderProps {
    children?: ReactNode;
}

export const MuiThemeProvider = ({ children }: MuiThemeProviderProps) => {
    const { themeMUI, setThemeMUI } = useThemeMui();
    const { theme } = useTheme();

    useEffect(() => {
        let newMuiTheme;
        switch (theme) {
            case Theme.DARK:
                newMuiTheme = darkTheme;
                break;
            case Theme.LIGHT:
                newMuiTheme = lightTheme;
                break;
            case Theme.ORANGE:
                newMuiTheme = orangeTheme;
                break;
            default:
                newMuiTheme = lightTheme;
        }
        setThemeMUI(newMuiTheme);
    }, [theme, setThemeMUI]);

    return <ThemeProvider theme={themeMUI}>{children}</ThemeProvider>;
};
