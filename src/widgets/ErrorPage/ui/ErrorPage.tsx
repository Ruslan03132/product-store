import { FC } from "react";
import { useTranslation } from "react-i18next";

import { classNames } from "shared/lib/classNames/classNames";
import { Button } from "shared/ui/Button/Button";

import styles from "./ErrorPage.module.scss";

interface ErrorPageProps {
    className?: string;
    error?: Error;
    resetErrorBoundary?: () => void;
}

export const ErrorPage: FC<ErrorPageProps> = ({
    className,
    error,
    resetErrorBoundary,
}) => {
    const [t] = useTranslation();
    return (
        <div className={classNames(styles.ErrorPage, {}, [className])}>
            <p>{t("Что-то пошло не так")}</p>
            <pre className={styles.message}>{error?.message}</pre>
            <Button className={styles.btnReset} onClick={resetErrorBoundary}>
                {t("Повторите снова")}
            </Button>
        </div>
    );
};
