import {
    Components,
    createTheme,
    PaletteOptions,
    ThemeOptions,
} from "@mui/material/styles";

import { Background } from "../../../shared/ui/Button/Button.stories";
import { MuiSelect } from "../../../shared/ui/Select/MuiSelect";

const global = {
    redLight: "rgb(255, 4, 4)",
};

const globalTypography = {
    fontSize: 14,
    fontFamily: ["Consolas", "Times New Roman", "serif"].join(","),
};

const globalComponents: Components = {};

const globalPalette = {};

const lightTheme = createTheme({
    global,
    palette: {
        ...globalPalette,
        mode: "light",
        primary: {
            main: "rgb(7 4 187)", // основной цвет для светлой темы
        },
        secondary: {
            main: "rgb(0 119 217)", // вторичный цвет для светлой темы
        },
        invertedPrimary: "rgb(77 234 4)",
        invertedSecondary: "rgb(7 116 1)",
        bgColor: "rgb(204 203 203)",
        invertedBgColor: "rgb(2 2 67)",
        skeletonColor: "#fff",
        skeletonShadow: "rgba(0 0 0 / 20%)",
        codeBg: "rgb(204 203 203)",
        // Добавьте другие цвета по необходимости
    },
    typography: {
        ...globalTypography,
    },
    components: {
        ...globalComponents,
        MuiPaper: {
            styleOverrides: {
                root: {
                    backgroundColor: "rgb(2 2 67)",
                },
            },
        },
    },
});

const darkTheme = createTheme({
    global,
    palette: {
        ...globalPalette,
        mode: "dark",
        primary: {
            main: "rgb(77 234 4)", // основной цвет для темной темы
        },
        secondary: {
            main: "rgb(7 116 1)", // вторичный цвет для темной темы
        },
        invertedPrimary: "rgb(7 4 187)",
        invertedSecondary: "rgb(0 119 217)",
        bgColor: "rgb(1 1 54)",
        invertedBgColor: "rgb(175 175 175)",
        skeletonColor: "#1515ad",
        skeletonShadow: "#2b2be8",
        codeBg: "rgb(1 1 54)",
        // Добавьте другие цвета по необходимости
    },
    typography: {
        ...globalTypography,
    },
    components: {
        ...globalComponents,
        MuiPaper: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: theme.palette.invertedBgColor,
                }),
            },
        },
    },
});

const orangeTheme = createTheme({
    global,
    palette: {
        ...globalPalette,
        primary: {
            main: "#c22113", // основной цвет для темной темы
        },
        secondary: {
            main: "#DF392A", // вторичный цвет для темной темы
        },
        invertedPrimary: "#e6b056",
        invertedSecondary: "#e6b056",
        bgColor: "#e6b056",
        invertedBgColor: "#FBF8F4",
        skeletonColor: "#fff",
        skeletonShadow: "rgba(0 0 0 / 20%)",
        codeBg: "#e6b056",
        // Добавьте другие цвета по необходимости
    },
    typography: {
        ...globalTypography,
    },
    components: {
        ...globalComponents,
        MuiPaper: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: theme.palette.invertedBgColor,
                }),
            },
        },
    },
});

export { lightTheme, darkTheme, orangeTheme };
