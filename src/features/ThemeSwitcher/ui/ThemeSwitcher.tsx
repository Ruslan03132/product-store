import { FC, memo } from "react";

import { useThemeMui } from "app/providers/MuiThemeProvider/libs/useThemeMui";
import { Theme, useTheme } from "app/providers/ThemeProvider";
import MyDarkIcon from "shared/assets/icons/theme-dark.svg";
import MyLightIcon from "shared/assets/icons/theme-light.svg";
import { classNames } from "shared/lib/classNames/classNames";
import { Button, ButtonTheme } from "shared/ui/Button/Button";

import styles from "./ThemeSwitcher.module.scss";

interface ThemeSwitcherProps {
    className?: string;
}

export const ThemeSwitcher = memo(({ className }: ThemeSwitcherProps) => {
    const { theme, toogleTheme } = useTheme();

    return (
        <Button
            theme={ButtonTheme.CLEAR}
            className={classNames(styles.ThemeSwitcher, {}, [className])}
            onClick={toogleTheme}
        >
            {theme === Theme.DARK ? <MyDarkIcon /> : <MyLightIcon />}
        </Button>
    );
});
