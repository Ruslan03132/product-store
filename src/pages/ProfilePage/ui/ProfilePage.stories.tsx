import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import avatar from "shared/assets/tests/testwoman.jpg";
import { StoreDecorator } from "shared/config/storybook/StoreDecorator/StoreDecorator";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import ProfilePage from "./ProfilePage";

const meta = {
    title: "pages/ProfilePage",
    component: ProfilePage,
    tags: ["autodocs"],
} satisfies Meta<typeof ProfilePage>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ProfilePageLight: Story = {
    args: {},
    decorators: [
        StoreDecorator({
            profile: {
                form: {
                    avatar,
                    username: "admin",
                    age: 22,
                    country: Country.Armenia,
                    lastname: "borisvch",
                    first: "ruslan",
                    city: "acs",
                    currency: Currency.EUR,
                },
            },
        }),
    ],
};

export const ProfilePageDark: Story = {
    args: {},
    decorators: [
        ThemeDecorator(Theme.DARK),
        StoreDecorator({
            profile: {
                form: {
                    avatar,
                    username: "admin",
                    age: 22,
                    country: Country.Armenia,
                    lastname: "borisvch",
                    first: "ruslan",
                    city: "acs",
                    currency: Currency.EUR,
                },
            },
        }),
    ],
};
