import { memo, useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { styled } from "@mui/material";

import {
    ArticleBlock,
    ArticleBlockType,
} from "entities/Article/model/types/article";
import CalendarIcon from "shared/assets/icons/calendar-20-20.svg";
import EyeIcon from "shared/assets/icons/eye-20-20.svg";
import {
    DynamicModuleLoader,
    ReducersList,
} from "shared/lib/components/DynamicModuleLoader/DynamicModuleLoader";
import { useAppDispatch } from "shared/lib/hooks/useAppDispatch/useAppDispatch";
import { Avatar } from "shared/ui/Avatar/Avatar";
import { Icon } from "shared/ui/Icon/Icon";
import { Loader } from "shared/ui/Loader/Loader";
import { Skeleton } from "shared/ui/Skeleton/Skeleton";
import { Text, TextAlign, TextSize } from "shared/ui/Text/Text";

import {
    getArticleDetailsData,
    getArticleDetailsError,
    getArticleDetailsIsLoading,
} from "../../model/selectors/articleDetails";
import { fetchArticleById } from "../../model/services/fetchArticleById/fetchArticleById";
import { articleDetailsReducer } from "../../model/slice/articleDetailsSlice";
import { ArticleImageBlock } from "../../model/types/article";
import { ArticleCodeBlockComponent } from "../ArticleCodeBlockComponent/ArticleCodeBlockComponent";
import { ArticleImageBlockComponent } from "../ArticleImageBlockComponent/ArticleImageBlockComponent";
import { ArticleTextBlockComponent } from "../ArticleTextBlockComponent/ArticleTextBlockComponent";

interface ArticleDetailsProps {
    style?: React.CSSProperties;
    id: string;
}

const StyledArticleDetails = styled("div")({
    minHeight: "100%",
});

const reducers: ReducersList = {
    articleDetails: articleDetailsReducer,
};

const StyledSkeleton = styled(Skeleton)({
    marginTop: "15px",
});

const StyledAvatar = styled(Avatar)({
    display: "flex",
    justifyContent: "center",
    width: "100%",
    borderRadius: "50%",
});

const StyledIcon = styled("div")({
    marginRight: "8px",
});

const StyledIconWrap = styled("div")({
    display: "flex",
    alignItems: "center",
});

const StyledText = styled(Text)({});

const StyledWrapperForBlock = styled("div")({
    "& > *": {
        marginTop: "16px",
    },
});

// const StyledBlockComponent = styled()({});

const propsForAvatar = {
    sx: { margin: "0 auto" },
    width: 200,
    height: 200,
    border: "50%",
};

export const ArticleDetails = memo((props: ArticleDetailsProps) => {
    const { id, style } = props;

    const dispatch = useAppDispatch();

    const { t } = useTranslation("article");

    const isLoading = useSelector(getArticleDetailsIsLoading);
    const article = useSelector(getArticleDetailsData);
    const error = useSelector(getArticleDetailsError);

    const renderBlock = useCallback((block: ArticleBlock) => {
        switch (block.type) {
            case ArticleBlockType.CODE:
                return (
                    <ArticleCodeBlockComponent key={block.id} block={block} />
                );
            case ArticleBlockType.IMAGE:
                return (
                    <ArticleImageBlockComponent key={block.id} block={block} />
                );
            case ArticleBlockType.TEXT:
                return (
                    <ArticleTextBlockComponent key={block.id} block={block} />
                );
            default:
                return null;
        }
    }, []);

    useEffect(() => {
        if (__PROJECT__ !== "storybook") {
            // @ts-ignore
            dispatch(fetchArticleById(id));
        }
    }, [dispatch, id]);

    let content;

    if (isLoading) {
        content = (
            <div>
                <StyledSkeleton {...propsForAvatar} />
                <StyledSkeleton
                    sx={{ marginTop: "20px" }}
                    width={300}
                    height={32}
                />
                <StyledSkeleton width={600} height={24} />
                <StyledSkeleton width="100%" height={200} />
                <StyledSkeleton width="100%" height={200} />
            </div>
        );
    } else if (error) {
        content = (
            <Text
                align={TextAlign.CENTER}
                title={t("Произошла ошибка при загрузке статьи")}
            />
        );
    } else {
        content = (
            <>
                <StyledAvatar url={article?.img} avatarSize={200} />
                <StyledText
                    title={article?.title}
                    text={article?.subtitle}
                    size={TextSize.L}
                    sx={{ marginTop: "20px" }}
                />
                <StyledIconWrap>
                    <StyledIcon>
                        <Icon Svg={EyeIcon} />
                    </StyledIcon>
                    <Text text={String(article?.views)} />
                </StyledIconWrap>
                <StyledIconWrap>
                    <StyledIcon>
                        <Icon Svg={CalendarIcon} />
                    </StyledIcon>
                    <Text text={article?.createdAt} />
                </StyledIconWrap>
                <StyledWrapperForBlock>
                    {article?.blocks.map(renderBlock)}
                </StyledWrapperForBlock>
            </>
        );
    }
    return (
        <DynamicModuleLoader reducers={reducers} removeAfterUnmount>
            <StyledArticleDetails {...props}>{content}</StyledArticleDetails>
        </DynamicModuleLoader>
    );
});
