import { memo } from "react";
import { styled } from "@mui/material";

interface SkeletonProps {
    height?: string | number;
    width?: string | number;
    border?: string;
}

const StyledSkeleton = styled("div")(({ theme, ...props }) => ({
    "@keyframes load": {
        from: {
            left: "-150px",
        },
        to: {
            left: "100%",
        },
    },

    position: "relative",
    boxShadow: `0 2px 10px 0 ${theme.palette.skeletonShadow}`,
    overflow: "hidden",
}));

const StyledAnimation = styled("div")(({ theme }) => ({
    display: "block",
    position: "absolute",
    left: "-150px",
    height: "100%",
    width: "80%",
    background: `linear-gradient(to right, transparent 0%, ${theme.palette.skeletonColor} 50%, transparent 100%)`,
    animation: "load 1s cubic-bezier(0.4, 0, 0.2, 1) infinite",
}));

export const Skeleton = memo((props: SkeletonProps) => {
    const { height, width, border, ...otherprops } = props;
    return (
        <StyledSkeleton
            style={{ height, width, borderRadius: border }}
            {...otherprops}
        >
            <StyledAnimation />
        </StyledSkeleton>
    );
});
