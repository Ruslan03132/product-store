import webpack from "webpack";

import { buildBabelLoader } from "./loaders/buildBabelLoader";
import { buildCssLoader } from "./loaders/buildCssLoader";
import { BuildOptions } from "./types/config";

export function buildLoaders(options: BuildOptions): webpack.RuleSetRule[] {
    const { isDev } = options;

    const babelLoader = buildBabelLoader(options);

    const fileLoader = {
        test: /\.(png|jpe?g|gif)$/i,
        loader: "file-loader",
    };

    const svgLoader = {
        test: /\.svg$/,
        loader: "@svgr/webpack",
    };

    const cssLoader = buildCssLoader(isDev);

    const tsLoader = {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/,
    };

    return [fileLoader, svgLoader, babelLoader, tsLoader, cssLoader];
}
