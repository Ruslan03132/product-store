import { StateSchema } from "app/providers/StoreProvider";
import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getProfileData } from "./getProfileData";

describe("getProfileData", () => {
    test("should return data", () => {
        const data = {
            username: "admin",
            age: 22,
            country: Country.Armenia,
            lastname: "borisvch",
            first: "ruslan",
            city: "acs",
            currency: Currency.EUR,
        };
        const state: DeepPartial<StateSchema> = {
            profile: {
                data,
            },
        };
        expect(getProfileData(state as StateSchema)).toEqual(data);
    });
    test("should undefined", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getProfileData(state as StateSchema)).toEqual(undefined);
    });
});
