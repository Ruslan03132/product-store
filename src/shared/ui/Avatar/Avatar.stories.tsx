import type { Meta, StoryObj } from "@storybook/react";

import { Theme } from "app/providers/ThemeProvider";
import { ThemeDecorator } from "shared/config/storybook/ThemeDecorator/ThemeDecorator";

import { Avatar } from "./Avatar";

import "app/styles/index.scss";

const meta = {
    title: "shared/Avatar",
    component: Avatar,
    tags: ["autodocs"],
} satisfies Meta<typeof Avatar>;

export default meta;

type Story = StoryObj<typeof meta>;

export const AvatarDark: Story = {
    args: {
        avatarSize: 100,
        url: "https://t3.ftcdn.net/jpg/06/17/13/26/240_F_617132669_YptvM7fIuczaUbYYpMe3VTLimwZwzlWf.jpg",
    },
    decorators: [ThemeDecorator(Theme.DARK)],
};

export const AvatarLight: Story = {
    args: {
        avatarSize: 100,
        url: "https://t3.ftcdn.net/jpg/06/17/13/26/240_F_617132669_YptvM7fIuczaUbYYpMe3VTLimwZwzlWf.jpg",
    },
};
