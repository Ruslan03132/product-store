import { FC, memo } from "react";
import { Link, LinkProps } from "react-router-dom";

import { classNames } from "shared/lib/classNames/classNames";

import styles from "./AppLink.module.scss";

export enum AppLinkTheme {
    PRIMARY = "primary",
    INVERTED_PRIMARY = "inverted-primary",
    INVERTED_SECONDARY = "inverted-secondary",
}

interface AppLinkProps extends LinkProps {
    className?: string;
    theme?: AppLinkTheme;
}

export const AppLink = memo(({
    className,
    children,
    to,
    theme = AppLinkTheme.INVERTED_SECONDARY,
    ...otherProps
}: AppLinkProps) => (
    <Link
        className={classNames(styles.AppLink, {}, [className, styles[theme]])}
        to={to}
        {...otherProps}
    >
        {children}
    </Link>
));
