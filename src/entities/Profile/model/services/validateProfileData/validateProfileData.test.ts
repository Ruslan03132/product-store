import { Country } from "entities/Country";
import { Currency } from "entities/Currency";
import { userActions } from "entities/User";
import { TestAsyncThunk } from "shared/lib/tests/TestAsyncThunk/TestAsyncThunk";

import { validateProfileData } from "./validateProfileData";
import { ValidateProfileError } from "../../types/profile";

const data = {
    username: "admin",
    age: 22,
    country: Country.Armenia,
    lastname: "borisvch",
    first: "ruslan",
    city: "acs",
    currency: Currency.EUR,
};

describe("validateProfileData.test", () => {
    test("without data", async () => {
        const result = validateProfileData();
        expect(result).toEqual([ValidateProfileError.NO_DATA]);
    });

    test("without first and last name", async () => {
        const result = validateProfileData({
            ...data,
            first: "",
            lastname: "",
        });
        expect(result).toEqual([ValidateProfileError.INCORRECT_USER_DATA]);
    });

    test("incorrect age", async () => {
        const result = validateProfileData({
            ...data,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            age: "sefsef" as any,
        });
        expect(result).toEqual([ValidateProfileError.INCORRECT_AGE]);
    });

    test("incorrect country", async () => {
        const result = validateProfileData({
            ...data,
            country: undefined,
        });
        expect(result).toEqual([ValidateProfileError.INCORRECT_COUNTRY]);
    });

    test("incorrect all", async () => {
        const result = validateProfileData({});
        expect(result).toEqual([
            ValidateProfileError.INCORRECT_USER_DATA,
            ValidateProfileError.INCORRECT_AGE,
            ValidateProfileError.INCORRECT_COUNTRY,
            ValidateProfileError.NO_CITY,
        ]);
    });
});
