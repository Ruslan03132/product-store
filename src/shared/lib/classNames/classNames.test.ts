import { classNames } from "shared/lib/classNames/classNames";

describe("classNames", () => {
    test("with only first param", () => {
        expect(classNames("someClass")).toBe("someClass");
    });

    test("with additional class", () => {
        const expected = "someClass class1 class2";
        expect(classNames("someClass", {}, ["class1", "class2"])).toBe(
            expected,
        );
    });

    test("with mods", () => {
        const expected = "someClass class1 class2 hovered scrolable";
        expect(
            classNames("someClass", { hovered: true, scrolable: true }, [
                "class1",
                "class2",
            ]),
        ).toBe(expected);
    });

    test("with mods falsy values", () => {
        const expected = "someClass class1 class2";
        expect(
            classNames(
                "someClass",
                { hovered: false, scrolable: null, scrolable2: undefined },
                ["class1", "class2"],
            ),
        ).toBe(expected);
    });

    test("test", () => {
        const moc = jest.fn((n) => n * n);
        expect(moc(2)).toBe(4);
        expect(moc.mock.calls).toEqual([[2], [2]]);
        expect(((n) => 2 + 1 + n)(4)).toBe(7);
    });
});
