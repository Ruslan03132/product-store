import { render, screen } from "@testing-library/react";

import { Button, ButtonTheme } from "./Button";

describe("buttonTest", () => {
    test("test", () => {
        render(<Button>test</Button>);
        expect(screen.getByText("test")).toBeInTheDocument();
    });

    test("clear", () => {
        render(<Button theme={ButtonTheme.CLEAR}>CLEAR</Button>);
        expect(screen.getByText("CLEAR")).toHaveClass("clear");
    });
});
