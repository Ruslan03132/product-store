import { memo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { styled } from "@mui/material";

import { MuiSelect } from "shared/ui/Select/MuiSelect";

import { Country } from "../../model/types/country";

interface CountrySelectProps {
    value?: Country;
    onChange?: (value: Country) => void;
    readonly?: boolean;
}

const options = [
    { value: Country.Armenia, content: Country.Armenia },
    { value: Country.Belarus, content: Country.Belarus },
    { value: Country.Kazakhstan, content: Country.Kazakhstan },
    { value: Country.Russia, content: Country.Russia },
    { value: Country.Ukraine, content: Country.Ukraine },
];

export const CountrySelect = memo(
    ({ value, onChange, readonly }: CountrySelectProps) => {
        const { t } = useTranslation("profile");

        const onChangeHandler = useCallback(
            (value: string) => {
                onChange?.(value as Country);
            },
            [onChange],
        );

        return (
            <MuiSelect
                label={t("Укажите страну")}
                options={options}
                value={value}
                onChange={onChangeHandler}
                readonly={readonly}
            />
        );
    },
);
