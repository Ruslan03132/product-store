import { StateSchema } from "app/providers/StoreProvider";
import { DeepPartial } from "shared/lib/tests/deepPartial";

import { getLoginError } from "./getLoginError";

describe("getLoginError", () => {
    test("should return error", () => {
        const state: DeepPartial<StateSchema> = {
            loginForm: {
                error: "error",
            },
        };
        expect(getLoginError(state as StateSchema)).toEqual("error");
    });
    test("should empty", () => {
        const state: DeepPartial<StateSchema> = {};
        expect(getLoginError(state as StateSchema)).toEqual(undefined);
    });
});
