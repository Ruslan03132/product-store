import { memo } from "react";
import { styled } from "@mui/material";

interface IconProps {
    Svg: React.FunctionComponent<React.SVGAttributes<SVGElement>>;
}

const StyledIcon = styled("div")(({ theme }) => ({
    display: "flex",
    fill: theme.palette.primary.main,
}));

export const Icon = memo((props: IconProps) => {
    const { Svg } = props;
    return (
        <StyledIcon>
            <Svg />
        </StyledIcon>
    );
});
