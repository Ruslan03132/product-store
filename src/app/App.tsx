import { Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Navbar } from "widgets/Navbar";
import { PageLoader } from "widgets/PageLoader";
import { Sidebar } from "widgets/Sidebar";
import { getUserInited, userActions } from "entities/User";
import { classNames } from "shared/lib/classNames/classNames";

import { AppRouter } from "./providers/router";

import "./styles/index.scss";

const App = () => {
    const dispatch = useDispatch();
    const inited = useSelector(getUserInited);
    useEffect(() => {
        dispatch(userActions.initAuthData());
    }, [dispatch]);
    return (
        <div className={classNames("app", {}, [])} id="root">
            <Suspense fallback={<PageLoader />}>
                <Navbar />
                <div className="content-page">
                    <Sidebar />
                    <div className="page-wrapper">
                        {inited && <AppRouter />}
                    </div>
                </div>
            </Suspense>
        </div>
    );
};

export default App;
