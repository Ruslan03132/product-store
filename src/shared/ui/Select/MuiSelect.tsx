import { ChangeEvent, memo, useMemo, useState } from "react";
import TextField, { InputLabel, PaletteColor } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { styled } from "@mui/material/styles";

export interface SelectOption {
    value: string;
    content: string;
}

interface SelectProps {
    label?: string | null;
    options?: SelectOption[];
    value?: string;
    onChange?: (value: string) => void;
    readonly?: boolean;
}

const StyledSelectWrap = styled("div")({
    display: "flex",
});

const StyledInputLabel = styled(
    InputLabel,
    {},
)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
}));

const StyledSelectMUI = styled(
    Select,
    {},
)(({ theme }) => ({
    background: "none",
    outline: "none",
    border: "1px solid",
    borderColor: theme.palette.primary.main,
    color: theme.palette.primary.main,
    "& .Mui-disabled": {
        color: theme.palette.primary.main,
        "-webkit-text-fill-color": "unset",
    },
}));

const StyledMenuItem = styled(
    MenuItem,
    {},
)(({ theme }) => ({
    background: theme.palette.invertedBgColor,
    color: theme.palette.invertedPrimary,
}));

export const MuiSelect = memo((props: SelectProps) => {
    const {
        label = null,
        options,
        onChange,
        value,
        readonly,
        ...otherprops
    } = props;

    const optionList = useMemo(() => {
        return options?.map((opt) => (
            <StyledMenuItem value={opt.value} key={opt.value}>
                {opt.content}
            </StyledMenuItem>
        ));
    }, [options]);

    const onChangeHandler = (e: SelectChangeEvent) => {
        onChange?.(e.target.value as string);
    };

    return (
        <StyledSelectWrap {...otherprops}>
            {label && (
                <StyledInputLabel
                    sx={{
                        mr: 2,
                        color: "primary.main",
                        textAlign: "center",
                    }}
                    id="demo-multiple-chip-label"
                >
                    {label}
                </StyledInputLabel>
            )}
            <StyledSelectMUI
                labelId="demo-multiple-chip-label"
                id="demo-simple-select"
                value={value}
                label={label}
                // @ts-ignore
                onChange={onChangeHandler}
                disabled={readonly}
                variant="standard"
            >
                {optionList}
            </StyledSelectMUI>
        </StyledSelectWrap>
    );
});
