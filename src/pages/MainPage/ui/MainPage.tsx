import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

const StyledMainPage = styled.div``;

const MainPage = () => {
    const { t } = useTranslation("main");
    return <StyledMainPage>{t("Главная страница")}</StyledMainPage>;
};

export default MainPage;
